#!/bin/bash

version="nodata"

echo "Version number extracted from package.json : $version"

image_name="registry.gitlab.com/quentindrd/techtutor-database"

docker buildx build --provenance false --platform linux/amd64,linux/arm64 --tag "$image_name:$version" --push .