-- Insertion des données de test dans la table 'avatars'
INSERT INTO avatars (id, path)
VALUES
(1, '/avatars/avatar1.png'),
(2, '/avatars/avatar2.png');

-- Insertion des données de test dans la table 'users'
INSERT INTO users (id, lastname, firstname, civility, email, password, age, admin, id_avatar)
VALUES
(1, 'Dupont', 'Jean', 1, 'jean.dupont@email.com', 'pass123', 30, false, 1),
(2, 'Martin', 'Alice', 2, 'alice.martin@email.com', 'pass123', 25, true, 2);

-- Insertion des données de test dans la table 'courses'
INSERT INTO courses (id, title, short_description, long_description, difficulty, disabled, orderlist)
VALUES
(1, 'Introduction à la Programmation', 'Apprenez les bases de la programmation.', 'Ce cours couvre les fondamentaux de la programmation...', 1, false, 1),
(2, 'HTML pour Débutants', 'Commencez votre parcours en HTML.', 'Ce cours est conçu pour les débutants...', 2, false, 2);

-- Insertion des données de test dans la table 'resources'
INSERT INTO resources (id, id_courses, title, type, content, video_url, orderlist)
VALUES
(1, 1, 'Chapitre 1: Les Bases', 0, 'Contenu du chapitre 1', '', 1),
(2, 1, 'Chapitre 2: Variables', 1, 'Contenu du chapitre 2', 'http://exemple.com/video', 2);

-- Insertion des données de test dans la table 'quiz'
INSERT INTO quiz (id, question, id_courses, orderlist)
VALUES
(1, 'Quelle est la base de la programmation ?', 1, 1),
(2, 'Quest-ce que le HTML ?', 2, 1);

-- Insertion des données de test dans la table 'answer_quiz'
INSERT INTO answer_quiz (id, id_quiz, answer, is_correct)
VALUES
(1, 1, 'Les variables', true),
(2, 1, 'Les boucles', false),
(3, 2, 'Un langage de balisage', true),
(4, 2, 'Un langage de programmation', false);

-- Insertion des données de test dans la table 'games'
INSERT INTO games (id, id_courses, title, short_description, long_description, instructions, orderlist)
VALUES
(1, 1, 'Jeu de Programmation', 'Jeu basique de programmation.', 'Ce jeu aide à comprendre les concepts de base.', 'Suivez les instructions...', 1),
(2, 2, 'Jeu HTML', 'Jeu pour apprendre le HTML.', 'Ce jeu est conçu pour les débutants en HTML.', 'Suivez les étapes...', 1);

-- Insertion des données de test dans la table 'user_games'
INSERT INTO user_games (id, id_games, id_user, started_at, finish_at)
VALUES
(1, 1, 1, '2023-01-01', '2023-01-05'),
(2, 2, 2, '2023-02-01', '2023-02-05');

-- Insertion des données de test dans la table 'user_courses'
INSERT INTO user_courses (id, id_courses, id_user, started_at, finish_at)
VALUES
(1, 1, 1, '2023-01-01', '2023-01-10'),
(2, 2, 2, '2023-02-01', '2023-02-10');

-- Insertion des données de test dans la table 'user_resources'
INSERT INTO user_resources (id, id_resources, id_user, started_at, finish_at)
VALUES
(1, 1, 1, '2023-01-01', '2023-01-02'),
(2, 2, 2, '2023-02-01', '2023-02-02');

-- Insertion des données de test dans la table 'user_quiz'
INSERT INTO user_quiz (id, id_quiz, id_user, started_at, finish_at)
VALUES
(1, 1, 1, '2023-01-01', '2023-01-02'),
(2, 2, 2, '2023-02-01', '2023-02-02');
