CREATE DATABASE techtutor;

\c techtutor;
CREATE TABLE "users"(
    "id" BIGINT NOT NULL,
    "lastname" VARCHAR(255) NULL,
    "firstname" VARCHAR(255) NULL,
    "civility" INTEGER NULL,
    "email" VARCHAR(255) NULL,
    "password" VARCHAR(255) NULL,
    "age" INTEGER NULL,
    "admin" BOOLEAN NULL DEFAULT '0',
    "id_avatar" INTEGER NULL DEFAULT '0'
);
ALTER TABLE
    "users" ADD PRIMARY KEY("id");
CREATE TABLE "resources"(
    "id" BIGINT NOT NULL,
    "id_courses" BIGINT NULL,
    "title" VARCHAR(255) NULL,
    "type" INTEGER NULL DEFAULT '0',
    "content" TEXT NOT NULL,
    "video_url" VARCHAR(255) NOT NULL,
    "orderlist" INTEGER NULL
);
ALTER TABLE
    "resources" ADD PRIMARY KEY("id");
COMMENT
ON COLUMN
    "resources"."type" IS '0 = Text
1 = Video';
CREATE TABLE "courses"(
    "id" BIGINT NOT NULL,
    "title" VARCHAR(255) NULL,
    "short_description" TEXT NOT NULL,
    "long_description" TEXT NOT NULL,
    "difficulty" INTEGER NULL DEFAULT '1',
    "disabled" BOOLEAN NULL DEFAULT '0',
    "orderlist" INTEGER NULL
);
ALTER TABLE
    "courses" ADD PRIMARY KEY("id");
CREATE TABLE "user_games"(
    "id" BIGINT NOT NULL,
    "id_games" BIGINT NULL,
    "id_user" BIGINT NULL,
    "started_at" DATE NOT NULL,
    "finish_at" DATE NOT NULL
);
ALTER TABLE
    "user_games" ADD PRIMARY KEY("id");
CREATE TABLE "answer_quiz"(
    "id" BIGINT NOT NULL,
    "id_quiz" BIGINT NULL,
    "answer" VARCHAR(255) NULL,
    "is_correct" BOOLEAN NULL DEFAULT '0'
);
ALTER TABLE
    "answer_quiz" ADD PRIMARY KEY("id");
CREATE TABLE "user_resources"(
    "id" BIGINT NOT NULL,
    "id_resources" BIGINT NULL,
    "id_user" BIGINT NULL,
    "started_at" DATE NOT NULL,
    "finish_at" DATE NOT NULL
);
ALTER TABLE
    "user_resources" ADD PRIMARY KEY("id");
CREATE TABLE "quiz"(
    "id" BIGINT NOT NULL,
    "question" TEXT NULL,
    "id_courses" BIGINT NULL,
    "orderlist" INTEGER NULL
);
ALTER TABLE
    "quiz" ADD PRIMARY KEY("id");
CREATE TABLE "games"(
    "id" BIGINT NOT NULL,
    "id_courses" BIGINT NULL,
    "title" VARCHAR(255) NULL,
    "short_description" TEXT NOT NULL,
    "long_description" TEXT NOT NULL,
    "instructions" TEXT NOT NULL,
    "orderlist" INTEGER NULL
);
ALTER TABLE
    "games" ADD PRIMARY KEY("id");
CREATE TABLE "avatars"(
    "id" BIGINT NOT NULL,
    "path" VARCHAR(255) NULL
);
ALTER TABLE
    "avatars" ADD PRIMARY KEY("id");
CREATE TABLE "user_courses"(
    "id" BIGINT NOT NULL,
    "id_courses" BIGINT NULL,
    "id_user" BIGINT NULL,
    "started_at" DATE NOT NULL,
    "finish_at" DATE NOT NULL
);
ALTER TABLE
    "user_courses" ADD PRIMARY KEY("id");
CREATE TABLE "user_quiz"(
    "id" BIGINT NOT NULL,
    "id_quiz" BIGINT NULL,
    "id_user" BIGINT NULL,
    "started_at" DATE NOT NULL,
    "finish_at" DATE NOT NULL
);
ALTER TABLE
    "user_quiz" ADD PRIMARY KEY("id");
ALTER TABLE
    "user_games" ADD CONSTRAINT "user_games_id_user_foreign" FOREIGN KEY("id_user") REFERENCES "users"("id");
ALTER TABLE
    "answer_quiz" ADD CONSTRAINT "answer_quiz_id_quiz_foreign" FOREIGN KEY("id_quiz") REFERENCES "quiz"("id");
ALTER TABLE
    "user_courses" ADD CONSTRAINT "user_courses_id_courses_foreign" FOREIGN KEY("id_courses") REFERENCES "courses"("id");
ALTER TABLE
    "user_courses" ADD CONSTRAINT "user_courses_id_user_foreign" FOREIGN KEY("id_user") REFERENCES "users"("id");
ALTER TABLE
    "user_resources" ADD CONSTRAINT "user_resources_id_resources_foreign" FOREIGN KEY("id_resources") REFERENCES "resources"("id");
ALTER TABLE
    "user_quiz" ADD CONSTRAINT "user_quiz_id_quiz_foreign" FOREIGN KEY("id_quiz") REFERENCES "quiz"("id");
ALTER TABLE
    "resources" ADD CONSTRAINT "resources_id_courses_foreign" FOREIGN KEY("id_courses") REFERENCES "courses"("id");
ALTER TABLE
    "user_resources" ADD CONSTRAINT "user_resources_id_user_foreign" FOREIGN KEY("id_user") REFERENCES "users"("id");
ALTER TABLE
    "games" ADD CONSTRAINT "games_id_courses_foreign" FOREIGN KEY("id_courses") REFERENCES "courses"("id");
ALTER TABLE
    "user_quiz" ADD CONSTRAINT "user_quiz_id_user_foreign" FOREIGN KEY("id_user") REFERENCES "users"("id");
ALTER TABLE
    "quiz" ADD CONSTRAINT "quiz_id_courses_foreign" FOREIGN KEY("id_courses") REFERENCES "courses"("id");
ALTER TABLE
    "user_games" ADD CONSTRAINT "user_games_id_games_foreign" FOREIGN KEY("id_games") REFERENCES "games"("id");
ALTER TABLE
    "users" ADD CONSTRAINT "users_id_avatar_foreign" FOREIGN KEY("id_avatar") REFERENCES "avatars"("id");